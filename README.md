# AngularBase

Es un repositorio que contiene el proyecto base utilizando la arquitectura planteada. 
Fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 10.1.7.
Debe utilizar una versión de Node 10.13 o superior.


## Primeros pasos
1. Descargar el respositorio
2. Renombrar la carpeta.
3. Ingresar a la carpeta.
4. Ejecutar npm install para instalar las dependencias.
5. Realizar las modificaciones de acuerdo a su proyecto.
    - Con ng generate se generarán los elementos angular.
6. Ejecutar ng serve para compilar y correr la aplicación en el servidor local. (Con ng serve -o se abrirá en una pestaña del navegador)
7. Ejecutar ng test para correr los tests.
7. Ejecutar ng build --prod para construir la aplicación y desplegar en producción. Se creará la carpeta dist con lo compilado.

## Descripción de las ramas

