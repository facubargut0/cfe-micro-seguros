import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from "@angular/common/http";
import { Observable, pipe } from 'rxjs';
import { tap, timeout } from "rxjs/operators";
import { Injectable } from '@angular/core';
import { TokenService } from '../../token/token.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor( private tokenService: TokenService) {}

  public intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        Authorization: 'Bearer ' + this.tokenService.getToken()
      }
    });
    
    return next.handle(req)
    // Intercepta la respuesta de vuelta a la app. Este 'pipe' la respuesta a traves del operador 'tap', 
    //   cuyo callback realiza una taera.
    // La respuesta original continua sin tocar de retorno en la cadena de interceptores de la aplicacion que llamo.
    .pipe(
      timeout(30000),
      tap({
        error: error => { 
          if (error && error.status == 401 ){
            this.tokenService.caducarToken();
           }},
        complete: () => { }
      })
    );
  }
}