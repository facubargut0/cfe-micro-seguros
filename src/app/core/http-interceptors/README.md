# Modo de uso

import { httpInterceptorProviders } from './core/http-interceptors/index';

@NgModule({
  declarations: [
      ...
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule {
}
