import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
//Primeng Modules
import { ButtonModule } from 'primeng/button';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, AppRoutingModule, ButtonModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
