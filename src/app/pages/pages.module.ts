import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';

import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';

//Componentes
import { ProductosComponent } from './productos/productos.component';
import { PlanesComponent } from './planes/planes.component';
import {ProgressBarModule} from 'primeng/progressbar';
import {StepsModule} from 'primeng/steps';



@NgModule({
  declarations: [ProductosComponent, PlanesComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    CardModule,
    ButtonModule,
    ProgressBarModule,
    StepsModule,
    
  ]
})
export class PagesModule { }
