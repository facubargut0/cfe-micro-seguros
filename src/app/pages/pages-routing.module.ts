import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PlanesComponent } from './planes/planes.component';
import { ProductosComponent } from './productos/productos.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'productos',
        component: ProductosComponent
      },
      {
        path:'planes',
        component: PlanesComponent
      },
      {
        path: '**',
        component: PlanesComponent
      }
    ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
