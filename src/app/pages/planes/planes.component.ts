import { Component, OnInit } from '@angular/core';

export enum PageNames {
  TipoCobertura,
  DocumentacionPersonal,
  DocumentacionRespaldatoria,
  Pago
}


@Component({
  selector: 'ma-planes',
  templateUrl: './planes.component.html',
  styleUrls: ['./planes.component.scss']
})
export class PlanesComponent implements OnInit {
  coberturas: any[] = [];
  items: any[];

  PageNames = PageNames;
  dialogPageIndex = 0;



  constructor() { }

  ngOnInit(): void {
    this.dialogPageIndex = PageNames.TipoCobertura
    console.log(PageNames)

    this.items = [
      {label: 'Tipo de Cobertura'},
      {label: 'Documentación personal'},
      {label: 'Documentación respaldatoria'},
      {label: 'Pago'}
  ];


    this.coberturas = [
      {
        titulo: "Plan 1",
        suma_asegurada: "50.000",
        cuota: "475",
        franquicia: "20",
      },
      {
        titulo: "Plan 2",
        suma_asegurada: "75.000",
        cuota: "700",
        franquicia: "20",
      },
      {
        titulo: "Plan 3",
        suma_asegurada: "100.000",
        cuota: "928",
        franquicia: "20",
      },
      {
        titulo: "Plan 4",
        suma_asegurada: "130.000",
        cuota: "1190",
        franquicia: "20",
      },
      {
        titulo: "Plan 5",
        suma_asegurada: "160.000",
        cuota: "1455",
        franquicia: "20",
      }
    ]
  }

  hiddenBackButton():boolean{
    if(this.dialogPageIndex != 0){
      return true
    }

    return false;
  }

  headerCard():string{
    switch (this.dialogPageIndex) {
      case 0:
        return "Seleccioná el tipo de cobertura"
        break;
      case 1:
        return "Completá la documentación personal"
        break;
      case 2:
        return "Completá la documentacion respaldatoria"
        break;
      case 3:
        return "Pago"
        break;
    }
  }

  seleccionarPlan(){
    this.dialogPageIndex ++
  }

}
