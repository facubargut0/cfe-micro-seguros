import { Component, OnInit } from '@angular/core';
import { Route,Router,RouterModule } from '@angular/router';

@Component({
  selector: 'ma-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  constructor( private router: Router) { }
  productos: any[] = [];

  ngOnInit(): void {
    this.productos = [
      {
        titulo: 'Celulares',
        descripcion: 'Cobertura por robo y por robo y daño de celulares, smartphones y smartwatches.',
        icon: '../../../assets/icons/phone.png',
        activado: true,
      },
      {
        titulo: 'Tecno portátiles',
        descripcion: 'Estamos trabajando para que puedas cotizar seguros para tecnología portátil.',
        icon: '../../../assets/icons/laptop.png',
        activado: false,
      },
      {
        titulo: 'Bolso protegido',
        descripcion: 'Estamos trabajando para que puedas cotizar seguros para tecnología portátil.',
        icon: '../../../assets/icons/bag.png',
        activado: false,
      },
      
    ]

    console.log(this.productos)
  }


  mostrarPlanesProducto(productoActivado:Boolean){
    if(productoActivado != true){
      return false;
    }

    this.router.navigate(['/planes'])
  }

  goSigma(){
    alert("Al tocar este boton, se debería redirigir a SIGMA")
  }

}
