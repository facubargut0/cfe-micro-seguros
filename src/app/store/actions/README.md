# Actions

Se listan las acciones de todo el proyecto. Las acciones son utilizadas para enviar datos desde el componente al store.

## Ejemplo:

````html
import { createAction, props } from '@ngrx/store';

export const cargarUsuarios = createAction('[Usuarios] Cargar Usuarios');

export const cargarUsuariosExito = createAction('[Usuarios] Cargar Usuarios Exito', props<{ usuarios: Usuario[] }>());

export const cargarUsuariosError = createAction('[Usuarios] Cargar Usuarios Error', props<{ payload: any }>());
````

## Recomendación
Es recomendable utilizar un index para exportar todas las acciones.