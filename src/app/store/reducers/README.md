# Reducers

Listado de reducers de la aplicación.

Las acciones describen que algo pasó, pero no especifican cómo cambió el estado de la aplicación en respuesta. Esto es trabajo de los reducers.

## Ejemplo

````html
//Estado inicial:
const usuarioInitialState: UsuarioState = {
  id: null,
  user: null,
  loaded: false,
  loading: false,
  error: null
};

//Reducer
export const usuarioReducer = createReducer(
  usuarioInitialState,
  on(cargarUsuario, (state, { id }) => ({ ...state, loading: true, id: id })),
  on(cargarUsuarioExito, (state, { usuario }) => ({
    ...state,
    loading: false,
    loaded: true,
    user: {...usuario}
  })),
  on(cargarUsuarioError, (state, { payload }) => ({
    ...state,
    loading: false,
    loaded: false,
    error: {
      url: payload.url,
      name: payload.name,
      message: payload.message
    }
  }))
);
````

## Recomendación
Es recomendable utilizar un index para exportar todas los reducers.