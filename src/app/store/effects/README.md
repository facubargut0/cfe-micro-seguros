# Effects

Listado de los effects de la aplicación.

Son los encargados de los efectos secundarios que provocan las las instrucciones asíncronas. De una forma simplista, diremos que las acciones asíncronas se multiplicarán por tres. El comando que genera la llamada, y los dos potenciales eventos con la respuesta correcta o el error.

Es necesario incluir el modulo: EffectsModule

ng add @ngrx/effects
EffectsModule.forFeature(EffectsArray)

## Ejemplo: 

````html
 cargarUsuario$ = createEffect(
        () => this.actions$.pipe(
            ofType(cargarUsuario),
            mergeMap(
                ( action ) => this.usuariosService.getUsersById(action.id)
                .pipe(
                  map( user => cargarUsuarioExito({ usuario: user})),
                  catchError( err => of(cargarUsuarioError({payload: err})) )
                )
            )
        )
    );
````


## Recomendación
Es recomendable utilizar un index para exportar todas los efectos dentro de un array para luego importarlo en el module.
export const EffectsArray: any[] = [UsuariosEffects, UsuarioEffects];