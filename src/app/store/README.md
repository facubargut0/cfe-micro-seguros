# Redux

## Esta carpeta es opcional, utilizar en caso de usar Redux, caso contrario eliminar.

En Redux, todo el estado de la aplicación es almacenado en un único objeto que solo puede ser modificado mediante acciones.

## Estructura para utilizar Redux con NgRx
- Reducer principal que se encarga de manipular el arbol de state y declarar todos los reducers internos.
- Reducers
- Actions
- Effects

## Instalación

Store:
npm install @ngrx/store --save

DevTools:
npm install @ngrx/store-devtools --save

Effects:
npm install @ngrx/effects --save
