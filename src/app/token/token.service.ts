import { Injectable } from '@angular/core';
import { from } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class TokenService {

    private token;
    private _promise;
    private _observer ;
    private _resolve;

    constructor(){
        //console.log('>> TokenService -> Constructor');
        this._promise = new Promise((resolve, reject) => { this._resolve = resolve });
//        this._observer = new Observable(this.tokenSubscriber);
        this._observer = from(this._promise);
    }
    removeToken(){}
    getToken(){
        return this.token;
    }
    setToken(t){
        this.token = t;
    }
    caducarToken(){
        this.token = undefined;
        this._resolve();
    }

    // Notifica que se ha caducado el token mediante un Observer
    caducoToken(){
        return this._observer;
    }

}