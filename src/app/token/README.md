# Obejtivo
Mantiene el token y notifica cuando se notifica que caduco el mismo.

# Modo de uso

tokenService.setToken('my_token');
console.log('>> Token:' + tokenService.getToken());
tokenService.caducoToken().subscribe({complete(){console.log('se completo la suscripcion por token caduco')}});
setTimeout( () => { console.log(' >> caduco el token'); tokenService.caducarToken(); } , 7000);