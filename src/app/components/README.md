# Funcionalidad
Lista de componentes generales, reutilizables por todos los componentes que contenga el proyecto

# Estructura de un componente:
- HTML
- Modulo
- Modelo
- Servicio **
- SCSS
- README.md
- Assets/imagenes 
** Poner lógica en los servicios y no manipular la información recibida en el controlador. 